package btester.domain;

import java.util.ArrayList;
import java.util.List;

public class TestSuiteResult {
	
private List<TestResult> testsResults = new ArrayList<>();
	
	private TestVerification finalTestVerification;
	
	private float totalTime;
	
	private int totalUnitTests;
	
	private int unitTestSuccess; 
	
	private int unitTestFault;

	public List<TestResult> getTestsResults() {
		return testsResults;
	}

	public void setTestsResults(List<TestResult> testsResults) {
		this.testsResults = testsResults;
	}

	public TestVerification getFinalTestVerification() {
		return finalTestVerification;
	}

	public void setFinalTestVerification(TestVerification finalTestVerification) {
		this.finalTestVerification = finalTestVerification;
	}

	public float getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(float totalTime) {
		this.totalTime = totalTime;
	}

	public int getTotalUnitTests() {
		return totalUnitTests;
	}

	public void setTotalUnitTests(int totalUnitTests) {
		this.totalUnitTests = totalUnitTests;
	}

	public int getUnitTestSuccess() {
		return unitTestSuccess;
	}

	public void setUnitTestSuccess(int unitTestSuccess) {
		this.unitTestSuccess = unitTestSuccess;
	}

	public int getUnitTestFault() {
		return unitTestFault;
	}

	public void setUnitTestFault(int unitTestFault) {
		this.unitTestFault = unitTestFault;
	} 
	
	
}
