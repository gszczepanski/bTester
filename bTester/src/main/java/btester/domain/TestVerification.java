package btester.domain;

public enum TestVerification {
	SUCCESS(true),
	FAILURE(false);
	
	private boolean verification;
	
	private TestVerification(boolean verification){
		this.verification = verification;
	}
	
	public boolean getVerification(){
		return verification;
	}
	
}
