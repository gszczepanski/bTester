package btester.domain;

import java.util.Optional;

public class TestMethodResult {
	
	private String name;
	
	private TestVerification testVerification;
	
	private float time;
	
	private Optional<Exception> exception;
	
	public TestMethodResult(String name, TestVerification testVerification, float time, Optional<Exception> exception){
		this.name = name;
		this.testVerification = testVerification;
		this.time = time;
		this.exception = exception;
	}

	public String getName() {
		return name;
	}

	public TestVerification getTestVerification() {
		return testVerification;
	}

	public float getTime() {
		return time;
	}

	public Optional<Exception> getException() {
		return exception;
	}
}
