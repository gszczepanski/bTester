package btester.domain.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import btester.framework.exceptions.DefaultException;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface UnitTest {
	
	public long timeout() default -1;
	
	public Class<? extends Throwable> expectedException() default DefaultException.class;
	
}
