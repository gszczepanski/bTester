package btester.domain;

import java.util.List;

public class TestResult {
	
	private final String testName;
	
	private final TestVerification verification;
	
	private final float totalTime;
	
	private final List<TestMethodResult> methodsResults;

	public TestResult(String testName, TestVerification verification, float totalTime,
			List<TestMethodResult> methodsResults) {
		this.testName = testName;
		this.verification = verification;
		this.totalTime = totalTime;
		this.methodsResults = methodsResults;
	}

	public String getTestName() {
		return testName;
	}

	public TestVerification getVerification() {
		return verification;
	}

	public float getTotalTime() {
		return totalTime;
	}

	public List<TestMethodResult> getMethodsResults() {
		return methodsResults;
	}

}
