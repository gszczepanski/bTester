package btester.domain.dto;

import java.util.List;

import btester.domain.TestMethodResult;

public class TestClassResultDto {

	private final String testClassName;

	private final List<TestMethodResult> testMethodResults;

	public TestClassResultDto(String testClassName, List<TestMethodResult> testMethodResults) {
		this.testClassName = testClassName;
		this.testMethodResults = testMethodResults;
	}

	public String getTestClassName() {
		return testClassName;
	}

	public List<TestMethodResult> getTestMethodResults() {
		return testMethodResults;
	}

}
