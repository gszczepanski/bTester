package btester.domain.dto;

import java.lang.reflect.Method;
import java.util.Set;

public class TestClassData {

	private Object testObject;

	private Set<Method> testMethods;

	private Method beforeTestMethod;

	private Method beforeMethod;

	private Method afterTestMethod;

	private Method afterMethod;

	public Object getTestObject() {
		return testObject;
	}

	public void setTestObject(Object testObject) {
		this.testObject = testObject;
	}

	public Set<Method> getTestMethods() {
		return testMethods;
	}

	public void setTestMethods(Set<Method> testMethods) {
		this.testMethods = testMethods;
	}

	public Method getBeforeTestMethod() {
		return beforeTestMethod;
	}

	public void setBeforeTestMethod(Method beforeTestMethod) {
		this.beforeTestMethod = beforeTestMethod;
	}

	public Method getBeforeMethod() {
		return beforeMethod;
	}

	public void setBeforeMethod(Method beforeMethod) {
		this.beforeMethod = beforeMethod;
	}

	public Method getAfterTestMethod() {
		return afterTestMethod;
	}

	public void setAfterTestMethod(Method afterTestMethod) {
		this.afterTestMethod = afterTestMethod;
	}

	public Method getAfterMethod() {
		return afterMethod;
	}

	public void setAfterMethod(Method afterMethod) {
		this.afterMethod = afterMethod;
	}

}
