package btester.domain;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import btester.framework.exceptions.BTesterRuntimeException;

public enum PrintElement {
	
	TEST_HEADER("test.header"),
	TEST_TIME("test.time"),
	TEST_AMOUNT("test.amount"),
	TEST_TESTS_SUCCESS("test.testsSuccess"),
	TEST_TESTS_FAILURES("test.testsFailures"),
	TEST_VERIFICATION("test.verification"),
	TEST_CLASS_NAME("testClass.name"),
	TEST_CLASS_VERIFICATION("testClass.verification"),
	TEST_CLASS_TIME("testClass.time"),
	UNIT_TEST_HEADER("unitTest.header"),
	UNIT_TEST_TIME("unitTest.time"),
	UNIT_TEST_VERIFICATION("unitTest.verification"),
	UNIT_TEST_STACK_TRACE("unitTest.stackTrace");
	
	private static Properties properties;
	
	private static final String PROPERTIES_PATH = "printerMessages.properties";
	
	static {
		loadProperties();
	}
	
	private String key;
	
	private PrintElement(String key) {
		this.key = key;
	}
	
	@Override
	public String toString(){
		return properties.getProperty(key);
	}
	
	private static void loadProperties() {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
    properties = new Properties();
    InputStream in = loader
            .getResourceAsStream(PROPERTIES_PATH);
    try {
        properties.load(in);
        in.close();
    } catch (IOException e) {
    	throw new BTesterRuntimeException(e);
    }
	}
}