package btester.framework;

import java.util.Optional;

import btester.domain.TestMethodResult;
import btester.domain.TestResult;
import btester.domain.TestSuiteResult;
import btester.framework.api.TestResultPrinter;

import static btester.domain.PrintElement.*;

public class TestResultPrinterImpl implements TestResultPrinter {
	
	public void print(TestSuiteResult testSuiteResult){
		System.out.println(TEST_HEADER);
		System.out.println("" + TEST_VERIFICATION + testSuiteResult.getFinalTestVerification());
		System.out.println("" + TEST_AMOUNT + testSuiteResult.getTotalUnitTests() + " (" + TEST_TESTS_SUCCESS
				+ testSuiteResult.getUnitTestSuccess() + " " + TEST_TESTS_FAILURES + testSuiteResult.getUnitTestFault() + ")");
		System.out.println("" + TEST_TIME + testSuiteResult.getTotalTime() + "s");
		System.out.println();
		
		for (TestResult testResult: testSuiteResult.getTestsResults()){
			printTestResult(testResult);
		}
	}
	
	private void printTestResult(TestResult testResult){
		System.out.println();
		System.out.println(TEST_CLASS_NAME + testResult.getTestName());
		System.out.println("" + TEST_CLASS_VERIFICATION + testResult.getVerification());
		System.out.println("" + TEST_CLASS_TIME + testResult.getTotalTime() + "s");
		
		for (TestMethodResult unitTestResult: testResult.getMethodsResults()){
			printUnitTestResult(unitTestResult);
		}
	}
	
	private void printUnitTestResult(TestMethodResult unitTestResult){
		System.out.println();
		System.out.println(UNIT_TEST_HEADER + unitTestResult.getName());
		System.out.println("" + UNIT_TEST_VERIFICATION + unitTestResult.getTestVerification());
		System.out.println("" + UNIT_TEST_TIME + unitTestResult.getTime() + "s");
		printStackTrace(unitTestResult.getException());
	}
	
	private void printStackTrace(Optional<Exception> exception){
		if (exception.isPresent()){
			exception.get().printStackTrace(System.out);
		}
	}
}
