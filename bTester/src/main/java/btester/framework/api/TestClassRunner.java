package btester.framework.api;

import btester.domain.dto.TestClassData;
import btester.domain.dto.TestClassResultDto;
import btester.framework.exceptions.BTesterRuntimeException;

public interface TestClassRunner {

	TestClassResultDto runTestClass(TestClassData testClassData) throws BTesterRuntimeException;
}
