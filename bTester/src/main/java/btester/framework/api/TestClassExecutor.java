package btester.framework.api;

import btester.domain.TestResult;

public interface TestClassExecutor {

	TestResult testTestClass(Class<?> testClass);
}
