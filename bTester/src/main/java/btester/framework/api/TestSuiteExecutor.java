package btester.framework.api;

import btester.domain.TestSuiteResult;
import btester.framework.TestSuite;

public interface TestSuiteExecutor {
	TestSuiteResult runTestSuite(TestSuite testSuite);
}
