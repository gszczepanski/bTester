package btester.framework.api;

public interface Timer {

	void start();

	void stop();

	float getTimeElapsedInSeconds();

	long getTimeElapsedInMilliseconds();

}
