package btester.framework.api;

import btester.domain.TestResult;
import btester.domain.TestSuiteResult;

public interface TestSuiteResultCollector {

	void addTestResult(TestResult testResult);

	TestSuiteResult getTestSuiteResult();
}
