package btester.framework.api;

import btester.domain.TestSuiteResult;

public interface TestResultPrinter {

	// TODO void setOutputStream(OutputStream outputStream);

	void print(TestSuiteResult testSuiteResult);

}
