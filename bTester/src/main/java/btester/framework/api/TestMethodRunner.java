package btester.framework.api;

import java.lang.reflect.Method;

import btester.domain.TestMethodResult;
import btester.framework.exceptions.BTesterRuntimeException;

public interface TestMethodRunner extends MethodRunner {

	TestMethodResult testMethod(Method method) throws BTesterRuntimeException;
}
