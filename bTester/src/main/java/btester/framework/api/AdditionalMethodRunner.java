package btester.framework.api;

import java.lang.reflect.Method;

import btester.framework.exceptions.BTesterRuntimeException;

public interface AdditionalMethodRunner extends MethodRunner {

	void runAdditionalMethod(Method method) throws BTesterRuntimeException;
}
