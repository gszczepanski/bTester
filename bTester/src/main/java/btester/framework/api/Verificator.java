package btester.framework.api;

public interface Verificator<T, R> {

	R verifyResult(T result);
}
