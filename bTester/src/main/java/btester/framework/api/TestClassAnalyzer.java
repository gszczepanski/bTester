package btester.framework.api;

import btester.domain.dto.TestClassData;

public interface TestClassAnalyzer {
	TestClassData analyzeTestClass(Class<?> testClass);
}
