package btester.framework;

import java.util.List;

import btester.domain.TestMethodResult;
import btester.domain.TestResult;
import btester.domain.TestVerification;
import btester.domain.dto.TestClassResultDto;
import btester.framework.api.Verificator;

public class TestClassVerificator implements Verificator<TestClassResultDto, TestResult> {

	@Override
	public TestResult verifyResult(TestClassResultDto result) {
		String testClassName = result.getTestClassName();
		List<TestMethodResult> testMethodResults = result.getTestMethodResults();
		float totalTime = getTotalTime(testMethodResults);
		TestVerification testVerification = getTestVerification(testMethodResults);
		return new TestResult(testClassName, testVerification, totalTime, testMethodResults);
	}

	private float getTotalTime(List<TestMethodResult> testMethodResults) {
		float totalTime = 0;
		for (TestMethodResult testMethodResult : testMethodResults) {
			totalTime += testMethodResult.getTime();
		}

		return totalTime;
	}

	private TestVerification getTestVerification(List<TestMethodResult> testMethodResults) {
		for (TestMethodResult testMethodResult : testMethodResults) {
			if (testMethodResult.getTestVerification() == TestVerification.FAILURE)
				return TestVerification.FAILURE;
		}

		return TestVerification.SUCCESS;
	}

}
