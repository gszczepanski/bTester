package btester.framework;

import java.util.List;

import btester.domain.TestMethodResult;
import btester.domain.TestResult;
import btester.domain.TestSuiteResult;
import btester.domain.TestVerification;
import btester.framework.api.Verificator;

public class TestSuiteVerificator implements Verificator<List<TestResult>, TestSuiteResult> {

	private TestVerification finalTestVerification;

	private float totalTime = 0;

	private int totalUnitTests = 0;

	private int unitTestSuccess = 0;

	private int unitTestFault = 0;

	@Override
	public TestSuiteResult verifyResult(List<TestResult> testResultList) {
		countTestStatistics(testResultList);
		return fillTestSuiteResult(testResultList);
	}

	private void countTestStatistics(List<TestResult> testResultList) {
		finalTestVerification = checkFinalTestVerification(testResultList);
		totalTime = countTotalTime(testResultList);
		countMethods(testResultList);
	}

	private TestVerification checkFinalTestVerification(List<TestResult> testResultList) {
		for (TestResult testResult : testResultList) {
			if (testResult.getVerification() == TestVerification.FAILURE)
				return TestVerification.FAILURE;
		}

		return TestVerification.SUCCESS;
	}

	private float countTotalTime(List<TestResult> testResultList) {
		for (TestResult testResult : testResultList) {
			totalTime += testResult.getTotalTime();
		}

		return totalTime;
	}

	private void countMethods(List<TestResult> testResultList) {
		for (TestResult testResult : testResultList) {
			for (TestMethodResult testMethodResult : testResult.getMethodsResults()) {
				if (testMethodResult.getTestVerification() == TestVerification.SUCCESS) {
					unitTestSuccess++;
				} else {
					unitTestFault++;
				}

				totalUnitTests++;
			}
		}
	}

	private TestSuiteResult fillTestSuiteResult(List<TestResult> testResultList) {
		TestSuiteResult testSuiteResult = new TestSuiteResult();

		testSuiteResult.setFinalTestVerification(finalTestVerification);
		testSuiteResult.setTestsResults(testResultList);
		testSuiteResult.setTotalTime(totalTime);
		testSuiteResult.setTotalUnitTests(totalUnitTests);
		testSuiteResult.setUnitTestFault(unitTestFault);
		testSuiteResult.setUnitTestSuccess(unitTestSuccess);

		return testSuiteResult;
	}

}
