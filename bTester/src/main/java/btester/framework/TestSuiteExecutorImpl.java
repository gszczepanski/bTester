package btester.framework;

import btester.domain.TestResult;
import btester.domain.TestSuiteResult;
import btester.framework.api.TestClassExecutor;
import btester.framework.api.TestSuiteExecutor;
import btester.framework.api.TestSuiteResultCollector;
import btester.framework.exceptions.BTesterRuntimeException;

public class TestSuiteExecutorImpl implements TestSuiteExecutor {

	private TestSuiteResultCollector resultCollector;

	private TestClassExecutor testClassExecutor;

	public TestSuiteExecutorImpl(TestSuiteResultCollector resultCollector, TestClassExecutor testClassExecutor) {
		this.resultCollector = resultCollector;
		this.testClassExecutor = testClassExecutor;
	}

	@Override
	public TestSuiteResult runTestSuite(TestSuite testSuite) {
		runTests(testSuite);
		return resultCollector.getTestSuiteResult();
	}

	private void runTests(TestSuite testSuite) throws BTesterRuntimeException {
		for (Class<?> testClass : testSuite.getTestSet()) {
			runTestClass(testClass);
		}
	}

	private void runTestClass(Class<?> testClass) throws BTesterRuntimeException {
		TestResult testResult = testClassExecutor.testTestClass(testClass);
		resultCollector.addTestResult(testResult);
	}

}
