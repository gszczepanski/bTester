package btester.framework;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import btester.domain.TestMethodResult;
import btester.domain.TestVerification;
import btester.domain.annotations.SecondChance;
import btester.domain.dto.TestClassData;
import btester.domain.dto.TestClassResultDto;
import btester.framework.api.AdditionalMethodRunner;
import btester.framework.api.TestClassRunner;
import btester.framework.api.TestMethodRunner;
import btester.framework.exceptions.BTesterRuntimeException;

class TestClassRunnerImpl implements TestClassRunner {

	private TestMethodRunner testMethodRunner;

	private AdditionalMethodRunner additionalMethodRunner;

	private List<TestMethodResult> testMethodResults;

	private Object testObject;

	private Set<Method> testMethods;

	private Method beforeTestMethod;

	private Method beforeMethod;

	private Method afterTestMethod;

	private Method afterMethod;

	public TestClassRunnerImpl(TestMethodRunner testMethodRunner, AdditionalMethodRunner additionalMethodRunner) {
		this.testMethodRunner = testMethodRunner;
		this.additionalMethodRunner = additionalMethodRunner;
	}

	@Override
	public TestClassResultDto runTestClass(TestClassData testClassData) throws BTesterRuntimeException {
		initializeTest(testClassData);
		runTest();
		return generateTestClassResultDto();
	}

	private void initializeTest(TestClassData testClassData) throws BTesterRuntimeException {
		clearFields();
		loadDataFromTestClassData(testClassData);
		setTestObjectToTestRunners();
	}

	private void clearFields() {
		testMethodResults = new ArrayList<>();
	}

	private void loadDataFromTestClassData(TestClassData testClassData) {
		this.testObject = testClassData.getTestObject();
		this.afterMethod = testClassData.getAfterMethod();
		this.afterTestMethod = testClassData.getAfterTestMethod();
		this.beforeMethod = testClassData.getBeforeMethod();
		this.beforeTestMethod = testClassData.getBeforeTestMethod();
		this.testMethods = testClassData.getTestMethods();
	}

	private void setTestObjectToTestRunners() {
		additionalMethodRunner.setTestObject(testObject);
		testMethodRunner.setTestObject(testObject);
	}

	private void runTest() throws BTesterRuntimeException {
		additionalMethodRunner.runAdditionalMethod(beforeTestMethod);
		runTestMethods();
		additionalMethodRunner.runAdditionalMethod(afterTestMethod);
	}

	private void runTestMethods() throws BTesterRuntimeException {
		testMethodResults = new ArrayList<>();

		for (Method testMethod : testMethods) {
			TestMethodResult testMethodResult = runTestMethod(testMethod);

			if (testMethodResult.getTestVerification() == TestVerification.FAILURE && isSecondChanceAnnotation(testMethod)) {
				testMethodResult = runTestMethod(testMethod);
			}

			testMethodResults.add(testMethodResult);
		}
	}

	private TestMethodResult runTestMethod(Method testMethod) throws BTesterRuntimeException {
		additionalMethodRunner.runAdditionalMethod(beforeMethod);
		TestMethodResult testMethodResult = testMethodRunner.testMethod(testMethod);
		additionalMethodRunner.runAdditionalMethod(afterMethod);

		return testMethodResult;
	}

	private TestClassResultDto generateTestClassResultDto() {
		String testClassName = testObject.getClass().getName();
		return new TestClassResultDto(testClassName, testMethodResults);
	}

	private boolean isSecondChanceAnnotation(Method method) {
		if (method.getAnnotation(SecondChance.class) == null) {
			return false;
		}

		return true;
	}

}
