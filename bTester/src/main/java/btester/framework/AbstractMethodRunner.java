package btester.framework;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

abstract class AbstractMethodRunner {
	
	private Object testObject;
	
	protected void runMethod(Optional<Method> method)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (method.isPresent()){
			Object[] args = {};
			method.get().invoke(testObject, args);
		}
	}
	
	public void setTestObject(Object testObject){
		this.testObject = testObject;
	}

}
