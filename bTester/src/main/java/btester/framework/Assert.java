package btester.framework;

import btester.framework.exceptions.AssertFailureException;

public class Assert {

	private static final String EXPECTED = "Expected: ";
	private static final String BUT_IT_WAS = ", but it was: ";
	private static final String NOT = "not ";

	public static void assertTrue(boolean condition) throws AssertFailureException {
		if (!condition)
			throw new AssertFailureException(EXPECTED + "true" + BUT_IT_WAS + !condition);
	}

	public static void assertNotTrue(boolean condition) throws AssertFailureException {
		if (condition)
			throw new AssertFailureException(EXPECTED + "false" + BUT_IT_WAS + condition);
	}

	public static void assertNull(Object object) throws AssertFailureException {
		if (object != null)
			throw new AssertFailureException(EXPECTED + "null" + BUT_IT_WAS + object);
	}

	public static void assertNotNull(Object object) throws AssertFailureException {
		if (object == null)
			throw new AssertFailureException(EXPECTED + "not Null" + BUT_IT_WAS + "null");
	}

	public static void assertEquals(Object expected, Object actual) throws AssertFailureException {
		if (!expected.equals(actual))
			throw new AssertFailureException(EXPECTED + expected + BUT_IT_WAS + actual);
	}

	public static void assertEquals(int expected, int actual) throws AssertFailureException {
		if (expected != actual)
			throw new AssertFailureException(EXPECTED + expected + BUT_IT_WAS + actual);
	}

	public static void assertEquals(long expected, long actual) throws AssertFailureException {
		if (expected != actual)
			throw new AssertFailureException(EXPECTED + expected + BUT_IT_WAS + actual);
	}

	public static void assertEquals(float expected, float actual) throws AssertFailureException {
		if (expected != actual)
			throw new AssertFailureException(EXPECTED + expected + BUT_IT_WAS + actual);
	}

	public static void assertEquals(char expected, char actual) throws AssertFailureException {
		if (expected != actual)
			throw new AssertFailureException(EXPECTED + expected + BUT_IT_WAS + actual);
	}

	public static void assertNotEquals(Object expected, Object actual) throws AssertFailureException {
		if (expected.equals(actual))
			throw new AssertFailureException(EXPECTED + NOT + expected + BUT_IT_WAS + actual);
	}

	public static void assertNotEquals(int expected, int actual) throws AssertFailureException {
		if (expected == actual)
			throw new AssertFailureException(EXPECTED + NOT + expected + BUT_IT_WAS + actual);
	}

	public static void assertNotEquals(long expected, long actual) throws AssertFailureException {
		if (expected == actual)
			throw new AssertFailureException(EXPECTED + NOT + expected + BUT_IT_WAS + actual);
	}

	public static void assertNotEquals(double expected, double actual) throws AssertFailureException {
		if (expected == actual)
			throw new AssertFailureException(EXPECTED + NOT + expected + BUT_IT_WAS + actual);
	}

	public static void assertNotEquals(float expected, float actual) throws AssertFailureException {
		if (expected == actual)
			throw new AssertFailureException(EXPECTED + NOT + expected + BUT_IT_WAS + actual);
	}

	public static void assertNotEquals(char expected, char actual) throws AssertFailureException {
		if (expected == actual)
			throw new AssertFailureException(EXPECTED + NOT + expected + BUT_IT_WAS + actual);
	}

	public static void assertReference(Object expected, Object actual) throws AssertFailureException {
		if (expected != actual)
			throw new AssertFailureException(EXPECTED + expected + BUT_IT_WAS + actual);
	}

	public static void assertNotReference(Object expected, Object actual) throws AssertFailureException {
		if (expected == actual)
			throw new AssertFailureException(EXPECTED + NOT + expected + BUT_IT_WAS + actual);
	}

}
