package btester.framework;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import btester.framework.api.AdditionalMethodRunner;
import btester.framework.exceptions.BTesterRuntimeException;

class AdditionalMethodRunnerImpl extends AbstractMethodRunner implements AdditionalMethodRunner{

	public void runAdditionalMethod(Method method) throws BTesterRuntimeException{
		try {
			Optional<Method> optionalMethod = Optional.ofNullable(method);
			runMethod(optionalMethod);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new BTesterRuntimeException(e);
		}
	}

}
