package btester.framework.exceptions;

public class TestTimeoutException extends Exception {

	private static final long serialVersionUID = -6920849315671410556L;

	private static final String DECLARED = "Declared timeout was:[";
	private static final String BUT = "s], but unit test take:[";
	private static final String END = "s]";

	public TestTimeoutException(long timeout, long unitTestTime) {
		super(DECLARED + timeout + BUT + unitTestTime + END);
	}

}
