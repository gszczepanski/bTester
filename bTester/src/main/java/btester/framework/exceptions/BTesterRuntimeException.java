package btester.framework.exceptions;

public class BTesterRuntimeException extends RuntimeException{

	private static final long serialVersionUID = 2503799749896827569L;

	public BTesterRuntimeException(Exception e) {
		super(e);
	}

	public BTesterRuntimeException() {
	}

}
