package btester.framework.exceptions;

public class ExpectedExceptionWasNotOccur extends Exception {

	private static final long serialVersionUID = -7583804644066417267L;

	private static final String MESSAGE = "Expected exception was not occur during method execution: ";

	public ExpectedExceptionWasNotOccur(Class<?> expectedExceptionClass) {
		super(MESSAGE + expectedExceptionClass.getName());
	}

}
