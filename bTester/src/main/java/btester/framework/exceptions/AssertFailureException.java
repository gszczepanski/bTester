package btester.framework.exceptions;

public class AssertFailureException extends Exception{

	private static final long serialVersionUID = -3428033960647147840L;

	public AssertFailureException(String message) {
		super(message);
	}

}
