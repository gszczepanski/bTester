package btester.framework;

import btester.domain.TestResult;
import btester.domain.dto.TestClassData;
import btester.domain.dto.TestClassResultDto;
import btester.framework.api.TestClassAnalyzer;
import btester.framework.api.TestClassExecutor;
import btester.framework.api.TestClassRunner;
import btester.framework.api.Verificator;

public class TestClassExecutorImpl implements TestClassExecutor {

	private final TestClassAnalyzer analyzer;

	private final TestClassRunner testClassRunner;

	private final Verificator<TestClassResultDto, TestResult> verificator;

	public TestClassExecutorImpl(TestClassAnalyzer analyzer, TestClassRunner testClassRunner,
			Verificator<TestClassResultDto, TestResult> verificator) {
		this.analyzer = analyzer;
		this.testClassRunner = testClassRunner;
		this.verificator = verificator;
	}

	@Override
	public TestResult testTestClass(Class<?> testClass) {
		TestClassData testClassData = analyzer.analyzeTestClass(testClass);
		TestClassResultDto dto = testClassRunner.runTestClass(testClassData);
		return verificator.verifyResult(dto);
	}

}
