package btester.framework;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import btester.domain.annotations.TestClass;
import btester.framework.exceptions.BTesterRuntimeException;

public class TestSuite {

	private Set<Class<?>> testSet;

	public TestSuite() {
		this.testSet = new HashSet<>();
	}

	public void addUnitTest(Class<?> cls) {
		checkIfClassHasTestAnnotation(cls);
		if (checkIfTestClassIsEnabled(cls)) {
			testSet.add(cls);
		}
	}

	public Set<Class<?>> getTestSet() {
		return testSet;
	}

	private void checkIfClassHasTestAnnotation(Class<?> cls) throws BTesterRuntimeException {
		if (cls.getAnnotation(TestClass.class) == null)
			throw new BTesterRuntimeException();
	}

	private boolean checkIfTestClassIsEnabled(Class<?> cls) {
		TestClass testClassAnnotation = cls.getAnnotation(TestClass.class);
		return testClassAnnotation.enabled();
	}

}
