package btester.framework;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import btester.domain.TestMethodResult;
import btester.domain.TestVerification;
import btester.domain.annotations.UnitTest;
import btester.framework.api.TestMethodRunner;
import btester.framework.api.Timer;
import btester.framework.exceptions.BTesterRuntimeException;
import btester.framework.exceptions.DefaultException;
import btester.framework.exceptions.ExpectedExceptionWasNotOccur;
import btester.framework.exceptions.TestTimeoutException;

class TestMethodRunnerImpl extends AbstractMethodRunner implements TestMethodRunner {

	private Timer timer;

	public TestMethodRunnerImpl(Timer timer) {
		this.timer = timer;
	}

	@Override
	public TestMethodResult testMethod(Method method) throws BTesterRuntimeException {
		return runTestMethod(method);
	}

	private TestMethodResult runTestMethod(Method method) throws BTesterRuntimeException {
		TestMethodResult testMethodResult = null;
		String unitTestName = method.getName();
		long testTimeout = loadTestTimeout(method);

		Class<? extends Throwable> expectedException = loadExpectedException(method);

		try {
			timer.start();
			runMethod(Optional.of(method));
			timer.stop();

			checkUnitTestTime(testTimeout);
			ifExpectedExceptionWasDeclaredThrowExpectedExceptionWasNotOccur(expectedException);

			testMethodResult = createTestMethodResult(unitTestName, TestVerification.SUCCESS, timer.getTimeElapsedInSeconds(),
					null);
		} catch (InvocationTargetException e) {
			timer.stop();

			TestVerification finalTestVerification = TestVerification.FAILURE;
			Class<?> exceptionThrowed = e.getCause().getClass();

			boolean declaredExceptionWasThrown = checkIfDeclaredExceptionWasThrown(expectedException, exceptionThrowed);

			if (declaredExceptionWasThrown) {
				finalTestVerification = TestVerification.SUCCESS;
				e = null;
			}

			testMethodResult = createTestMethodResult(unitTestName, finalTestVerification, timer.getTimeElapsedInSeconds(),
					e);
		} catch (TestTimeoutException | ExpectedExceptionWasNotOccur e) {

			testMethodResult = createTestMethodResult(unitTestName, TestVerification.FAILURE, timer.getTimeElapsedInSeconds(),
					e);
		} catch (IllegalAccessException | IllegalArgumentException e) {

			throw new BTesterRuntimeException(e);
		}

		return testMethodResult;
	}

	private TestMethodResult createTestMethodResult(String unitTestName, TestVerification testVerification,
			float unitTestTime, Exception exception) {
		return new TestMethodResult(unitTestName, testVerification, unitTestTime, Optional.ofNullable(exception));
	}

	private long loadTestTimeout(Method method) {
		UnitTest unitTestAnnotation = loadUnitTestAnnotation(method);
		return unitTestAnnotation.timeout();
	}

	private void checkUnitTestTime(long timeout) throws TestTimeoutException {
		long timeElapsedInMs = 0;
		timeElapsedInMs = timer.getTimeElapsedInMilliseconds();
		checkUnitTestTimeout(timeElapsedInMs, timeout);
	}

	private void checkUnitTestTimeout(long unitTestTime, long timeout) throws TestTimeoutException {
		if (timeout < 1) {
			return;
		} else if (timeout < unitTestTime) {
			throw new TestTimeoutException(timeout, unitTestTime);
		}
	}

	private Class<? extends Throwable> loadExpectedException(Method method) {
		UnitTest unitTestAnnotation = loadUnitTestAnnotation(method);
		return unitTestAnnotation.expectedException();
	}

	private void ifExpectedExceptionWasDeclaredThrowExpectedExceptionWasNotOccur(Class<?> expectedException)
			throws ExpectedExceptionWasNotOccur {
		if (expectedException != DefaultException.class) {
			throw new ExpectedExceptionWasNotOccur(expectedException);
		}
	}

	private boolean checkIfDeclaredExceptionWasThrown(Class<?> expectedException, Class<?> exceptionThrowed) {
		if (expectedException == exceptionThrowed) {
			return true;
		}
		return false;
	}

	private UnitTest loadUnitTestAnnotation(Method method) {
		return method.getAnnotation(UnitTest.class);
	}

}
