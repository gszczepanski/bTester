package btester.framework;

import btester.framework.api.Timer;

class TestTimer implements Timer {

	private static final int SECOND_IN_MILLISECONDS = 1000;

	private long startTime;

	private long stopTime;

	@Override
	public void start() {
		startTime = fetchTimeInMillis();
	}

	@Override
	public void stop() {
		stopTime = fetchTimeInMillis();
	}

	private long fetchTimeInMillis() {
		return System.currentTimeMillis();
	}

	@Override
	public float getTimeElapsedInSeconds() {
		return ((float) getTimeElapsedInMilliseconds() / SECOND_IN_MILLISECONDS);
	}

	@Override
	public long getTimeElapsedInMilliseconds() {
		return stopTime - startTime;
	}

}
