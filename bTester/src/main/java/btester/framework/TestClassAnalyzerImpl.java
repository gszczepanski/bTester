package btester.framework;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import btester.domain.annotations.AfterMethod;
import btester.domain.annotations.AfterTest;
import btester.domain.annotations.BeforeMethod;
import btester.domain.annotations.BeforeTest;
import btester.domain.annotations.UnitTest;
import btester.domain.dto.TestClassData;
import btester.framework.api.TestClassAnalyzer;
import btester.framework.exceptions.BTesterRuntimeException;

public class TestClassAnalyzerImpl implements TestClassAnalyzer {

	@Override
	public TestClassData analyzeTestClass(Class<?> testClass) {
		TestClassData testClassData = new TestClassData();

		setTestObject(testClass, testClassData);
		loadAnnotatedMethodsFromClass(testClass, testClassData);
		return testClassData;
	}

	private void setTestObject(Class<?> testClass, TestClassData testClassData) {
		Object testObject = getObjectFromClass(testClass);
		testClassData.setTestObject(testObject);
	}

	private Object getObjectFromClass(Class<?> testClass) throws BTesterRuntimeException {
		try {
			return testClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new BTesterRuntimeException(e);
		}
	}

	private void loadAnnotatedMethodsFromClass(Class<?> testClass, TestClassData testClassData) {
		Set<Method> testMethods = new HashSet<>();

		for (Method method : testClass.getMethods()) {
			if (method.getAnnotation(UnitTest.class) != null) {
				testMethods.add(method);
			} else if (method.getAnnotation(BeforeTest.class) != null) {
				testClassData.setBeforeTestMethod(method);
			} else if (method.getAnnotation(BeforeMethod.class) != null) {
				testClassData.setBeforeMethod(method);
			} else if (method.getAnnotation(AfterTest.class) != null) {
				testClassData.setAfterTestMethod(method);
			} else if (method.getAnnotation(AfterMethod.class) != null) {
				testClassData.setAfterMethod(method);
			}
		}

		testClassData.setTestMethods(testMethods);
	}

}
