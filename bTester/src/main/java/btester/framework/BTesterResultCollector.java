package btester.framework;

import java.util.ArrayList;
import java.util.List;

import btester.domain.TestResult;
import btester.domain.TestSuiteResult;
import btester.framework.api.TestSuiteResultCollector;
import btester.framework.api.Verificator;

class BTesterResultCollector implements TestSuiteResultCollector {

	private List<TestResult> testResultList = new ArrayList<>();

	private Verificator<List<TestResult>, TestSuiteResult> testSuiteVerificator;

	public BTesterResultCollector(Verificator<List<TestResult>, TestSuiteResult> testSuiteVerificator) {
		this.testSuiteVerificator = testSuiteVerificator;
	}

	@Override
	public void addTestResult(TestResult testResult) {
		testResultList.add(testResult);
	}

	@Override
	public TestSuiteResult getTestSuiteResult() {
		TestSuiteResult testSuiteResult = testSuiteVerificator.verifyResult(testResultList);
		clearResultCollector();
		return testSuiteResult;
	}

	private void clearResultCollector() {
		this.testResultList = new ArrayList<>();
	}

}
