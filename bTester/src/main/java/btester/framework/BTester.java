package btester.framework;

import java.util.List;

import btester.domain.TestResult;
import btester.domain.TestSuiteResult;
import btester.domain.TestVerification;
import btester.domain.dto.TestClassResultDto;
import btester.framework.api.AdditionalMethodRunner;
import btester.framework.api.TestClassAnalyzer;
import btester.framework.api.TestClassExecutor;
import btester.framework.api.TestClassRunner;
import btester.framework.api.TestMethodRunner;
import btester.framework.api.TestResultPrinter;
import btester.framework.api.TestSuiteExecutor;
import btester.framework.api.TestSuiteResultCollector;
import btester.framework.api.Timer;
import btester.framework.api.Verificator;
import btester.framework.exceptions.BTesterRuntimeException;

public class BTester {

	private static TestSuiteExecutor testSuiteExecutor;

	private static TestResultPrinter testResultPrinter;

	static {
		setUp();
	}

	public static boolean test(TestSuite testSuite) {
		try {
			TestSuiteResult testSuiteResult = testSuiteExecutor.runTestSuite(testSuite);
			showResult(testSuiteResult);
			return verifyResult(testSuiteResult.getFinalTestVerification());
		} catch (BTesterRuntimeException e) {
			e.printStackTrace();
			return false;
		}
	}

	private static void showResult(TestSuiteResult testSuiteResult) {
		testResultPrinter.print(testSuiteResult);
	}

	private static boolean verifyResult(TestVerification testVerification) {
		return testVerification.getVerification();
	}

	private static void setUp() {
		testResultPrinter = new TestResultPrinterImpl();

		Timer timer = new TestTimer();
		TestMethodRunner testMethodRunner = new TestMethodRunnerImpl(timer);
		AdditionalMethodRunner additionalMethodRunner = new AdditionalMethodRunnerImpl();

		TestClassAnalyzer testClassAnalyzer = new TestClassAnalyzerImpl();
		TestClassRunner testClassRunner = new TestClassRunnerImpl(testMethodRunner, additionalMethodRunner);
		Verificator<TestClassResultDto, TestResult> testClassVerificator = new TestClassVerificator();

		TestClassExecutor testClassExecutor = new TestClassExecutorImpl(testClassAnalyzer, testClassRunner,
				testClassVerificator);

		Verificator<List<TestResult>, TestSuiteResult> testSuiteVerificator = new TestSuiteVerificator();
		TestSuiteResultCollector resultCollector = new BTesterResultCollector(testSuiteVerificator);

		testSuiteExecutor = new TestSuiteExecutorImpl(resultCollector, testClassExecutor);
	}

}
