package btester.framework;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import btester.framework.api.TestClassExecutor;
import btester.framework.api.TestSuiteResultCollector;
import test.domain.AdditionalTestExample;
import test.domain.SimpleTestExample;

public class TestSuiteExecutorImplTest {

	@Mock
	TestClassExecutor testClassExecutor;

	@Mock
	TestSuiteResultCollector resultCollector;

	@InjectMocks
	TestSuiteExecutorImpl testSuiteExecutor;

	@BeforeMethod
	public void beforeMethod() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void runTestSuiteSuccess() {
		// Arrange
		TestSuite testSuite = new TestSuite();
		testSuite.addUnitTest(SimpleTestExample.class);
		testSuite.addUnitTest(AdditionalTestExample.class);

		// Act
		testSuiteExecutor.runTestSuite(testSuite);

		// Assert
		verify(testClassExecutor).testTestClass(SimpleTestExample.class);
		verify(testClassExecutor).testTestClass(AdditionalTestExample.class);
		verify(resultCollector, times(2)).addTestResult(any());
		verify(resultCollector).getTestSuiteResult();
	}

}
