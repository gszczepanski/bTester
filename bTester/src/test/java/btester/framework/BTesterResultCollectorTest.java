package btester.framework;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import btester.domain.TestResult;
import btester.domain.TestSuiteResult;
import btester.framework.api.Verificator;

public class BTesterResultCollectorTest {

	@InjectMocks
	BTesterResultCollector resultCollector;

	@Mock
	Verificator<List<TestResult>, TestSuiteResult> verificator;

	@BeforeMethod
	public void beforeMethod() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getTestSuiteResultTestSuccess() {
		// Act
		resultCollector.getTestSuiteResult();

		// Assert
		verify(verificator).verifyResult(any());
	}
}
