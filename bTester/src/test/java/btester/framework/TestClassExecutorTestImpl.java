package btester.framework;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import btester.domain.TestResult;
import btester.domain.TestVerification;
import btester.domain.dto.TestClassData;
import btester.domain.dto.TestClassResultDto;
import btester.framework.api.TestClassAnalyzer;
import btester.framework.api.TestClassRunner;
import btester.framework.api.Verificator;
import test.domain.SimpleTestExample;

public class TestClassExecutorTestImpl {

	@Mock
	TestClassAnalyzer testClassAnalyzer;

	@Mock
	TestClassRunner testClassRunner;

	@Mock
	Verificator<TestClassResultDto, TestResult> verificator;

	@InjectMocks
	TestClassExecutorImpl classExecutorImpl;

	@BeforeMethod
	public void beforeMethod() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testTestClassSuccess() {
		// Arrange
		Class<SimpleTestExample> testClass = SimpleTestExample.class;
		TestClassData testClassData = new TestClassData();
		TestClassResultDto dto = new TestClassResultDto("testTest", new ArrayList<>());
		TestResult expectedResult = new TestResult("testClass", TestVerification.SUCCESS, 0, new ArrayList<>());

		when(testClassAnalyzer.analyzeTestClass(testClass)).thenReturn(testClassData);
		when(testClassRunner.runTestClass(testClassData)).thenReturn(dto);
		when(verificator.verifyResult(dto)).thenReturn(expectedResult);

		// Act
		TestResult result = classExecutorImpl.testTestClass(testClass);

		// Assert
		Assert.assertEquals(result, expectedResult);

		verify(testClassAnalyzer).analyzeTestClass(testClass);
		verify(testClassRunner).runTestClass(testClassData);
		verify(verificator).verifyResult(dto);
	}
}
