package btester.framework;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import btester.domain.TestMethodResult;
import btester.domain.TestResult;
import btester.domain.TestSuiteResult;
import btester.domain.TestVerification;

public class TestSuiteVerificatorImplTest {

	TestSuiteVerificator testSuiteVerificator;

	@BeforeMethod
	public void beforeMethod() {
		testSuiteVerificator = new TestSuiteVerificator();
	}

	@Test
	public void testSuiteResultVerificationIsFailureWhenAtLeastOneTestIsFailure() {
		// Arrange
		long time = 222;

		List<TestMethodResult> unitTestResults = new ArrayList<>();
		unitTestResults.add(new TestMethodResult("test1", TestVerification.FAILURE, 2.300f, null));
		unitTestResults.add(new TestMethodResult("test2", TestVerification.SUCCESS, 6.300f, null));

		TestResult testResultFailure = new TestResult("failureTest", TestVerification.FAILURE, time, unitTestResults);
		TestResult testResultSuccess = new TestResult("successTest", TestVerification.SUCCESS, time, unitTestResults);

		List<TestResult> testResultList = new ArrayList<>();
		testResultList.add(testResultFailure);
		testResultList.add(testResultSuccess);

		// Act
		TestSuiteResult testSuiteResult = testSuiteVerificator.verifyResult(testResultList);

		// Assert
		org.testng.Assert.assertEquals(testSuiteResult.getFinalTestVerification(), TestVerification.FAILURE);
		org.testng.Assert.assertEquals(testSuiteResult.getTotalTime(), 444.0f);
		org.testng.Assert.assertEquals(testSuiteResult.getTotalUnitTests(), 4);
		org.testng.Assert.assertEquals(testSuiteResult.getUnitTestFault(), 2);
		org.testng.Assert.assertEquals(testSuiteResult.getUnitTestSuccess(), 2);
	}

	@Test
	public void testSuiteResultVerificationIsSuccessWhenAllTestResultsAreSuccess() {
		// Arrange
		long time = 222;

		List<TestMethodResult> unitTestResults = new ArrayList<>();
		unitTestResults.add(new TestMethodResult("test1", TestVerification.SUCCESS, 2.300f, null));
		unitTestResults.add(new TestMethodResult("test2", TestVerification.SUCCESS, 6.300f, null));

		TestResult testResultFailure = new TestResult("failureTest", TestVerification.SUCCESS, time, unitTestResults);
		TestResult testResultSuccess = new TestResult("successTest", TestVerification.SUCCESS, time, unitTestResults);

		List<TestResult> testResultList = new ArrayList<>();
		testResultList.add(testResultFailure);
		testResultList.add(testResultSuccess);

		// Act
		TestSuiteResult testSuiteResult = testSuiteVerificator.verifyResult(testResultList);

		// Assert
		org.testng.Assert.assertEquals(testSuiteResult.getFinalTestVerification(), TestVerification.SUCCESS);
		org.testng.Assert.assertEquals(testSuiteResult.getTotalTime(), 444.0f);
		org.testng.Assert.assertEquals(testSuiteResult.getTotalUnitTests(), 4);
		org.testng.Assert.assertEquals(testSuiteResult.getUnitTestFault(), 0);
		org.testng.Assert.assertEquals(testSuiteResult.getUnitTestSuccess(), 4);
	}

}
