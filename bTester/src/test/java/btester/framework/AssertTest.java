package btester.framework;

import org.testng.annotations.Test;

import btester.framework.exceptions.AssertFailureException;

public class AssertTest {

	@Test
	public void assertTrue() throws Exception{
		Assert.assertTrue(true);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertTrueThrowsException() throws Exception{
		Assert.assertTrue(false);
	}
	
	@Test
	public void assertNotTrue() throws Exception{
		Assert.assertNotTrue(false);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNotTrueThrowsException() throws Exception{
		Assert.assertNotTrue(true);
	}
	
	@Test
	public void assertNull() throws Exception{
		Assert.assertNull(null);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNullThrowsException() throws Exception{
		Assert.assertNull(new String());
	}
	
	@Test
	public void assertNotNull() throws Exception{
		Assert.assertNotNull(new String());
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNotNullThrowsException() throws Exception{
		Assert.assertNotNull(null);
	}
	
	@Test
	public void assertNotEqualsObject() throws Exception{
		String expected = "Expected";
		String actual = "Actual";
		Assert.assertNotEquals(expected, actual);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNotEqualsObjectThrowsException() throws Exception{
		String expected = "ExpectedAndActual";
		String actual = "ExpectedAndActual";
		Assert.assertNotEquals(expected, actual);
	}
	
	@Test
	public void assertNotEqualsInt() throws Exception{
		Assert.assertNotEquals(0, 1);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNotEqualsIntThrowsException() throws Exception{
		Assert.assertNotEquals(1, 1);
	}
	
	@Test
	public void assertNotEqualsLong() throws Exception{
		Assert.assertNotEquals(0L, 1L);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNotEqualsLongThrowsException() throws Exception{
		Assert.assertNotEquals(1L, 1L);
	}
	
	@Test
	public void assertNotEqualsDouble() throws Exception{
		Assert.assertNotEquals(0.0, 1.0);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNotEqualsDoubleThrowsException() throws Exception{
		Assert.assertNotEquals(1.0, 1.0);
	}
	
	@Test
	public void assertNotEqualsFloat() throws Exception{
		Assert.assertNotEquals(0.0F, 1.0F);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNotEqualsFloatThrowsException() throws Exception{
		Assert.assertNotEquals(0.0F, 0.0F);
	}
	
	@Test
	public void assertNotEqualsChar() throws Exception{
		Assert.assertNotEquals('b', 'a');
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNotEqualsCharThrowsException() throws Exception{
		Assert.assertNotEquals('a', 'a');
	}
	
	@Test
	public void assertEqualsObject() throws Exception{
		String expected = "ExpectedAndActual";
		String actual = "ExpectedAndActual";
		Assert.assertEquals(expected, actual);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertEqualsObjectThrowsException() throws Exception{
		String expected = "Expected";
		String actual = "Actual";
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void assertEqualsInt() throws Exception{
		Assert.assertEquals(1, 1);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertEqualsIntThrowsException() throws Exception{
		Assert.assertEquals(1, 0);
	}
	
	@Test
	public void assertEqualsLong() throws Exception{
		Assert.assertEquals(1L, 1L);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertEqualsLongThrowsException() throws Exception{
		Assert.assertEquals(1L, 0L);
	}
	
	@Test
	public void assertEqualsDouble() throws Exception{
		Assert.assertEquals(1.0, 1.0);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertEqualsDoubleThrowsException() throws Exception{
		Assert.assertEquals(1.0, 0.0);
	}
	
	@Test
	public void assertEqualsFloat() throws Exception{
		Assert.assertEquals(1.0F, 1.0F);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertEqualsFloatThrowsException() throws Exception{
		Assert.assertEquals(1.0F, 0.0F);
	}
	
	@Test
	public void assertEqualsChar() throws Exception{
		Assert.assertEquals('a', 'a');
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertEqualsCharThrowsException() throws Exception{
		Assert.assertEquals('a', 'b');
	}
	
	@Test
	public void assertReference() throws Exception{
		String expected = "abc";
		String actual = expected;
		Assert.assertReference(expected, actual);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertReferenceThrowsException() throws Exception{
		String expected = new String();
		String actual = new String();
		Assert.assertReference(expected, actual);
	}
	
	@Test
	public void assertNotReference() throws Exception{
		String expected = new String();
		String actual = new String();
		Assert.assertNotReference(expected, actual);
	}
	
	@Test(expectedExceptions = AssertFailureException.class)
	public void assertNotReferenceThrowsException() throws Exception{
		String expected = new String();
		String actual = expected;
		Assert.assertNotReference(expected, actual);
	}
	
	
}
