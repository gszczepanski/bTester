package btester.framework;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import btester.domain.TestMethodResult;
import btester.domain.TestVerification;
import btester.framework.api.Timer;
import test.domain.SimpleTestExample;

public class TestMethodRunnerImplTest {

	@InjectMocks
	TestMethodRunnerImpl testMethodRunner;

	@Mock
	Timer timer;

	Class<?> testClass = SimpleTestExample.class;
	float testTime = 1.0f;
	long testTimeInMillis = 1000L;

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		when(timer.getTimeElapsedInMilliseconds()).thenReturn(testTimeInMillis);
		when(timer.getTimeElapsedInSeconds()).thenReturn(testTime);
	}

	@Test
	public void runTestMethodWithCorrectUnitTestSuccess() throws Exception {
		// Arrange
		String methodName = "powSuccess";
		Object testObject = testClass.newInstance();
		testMethodRunner.setTestObject(testObject);
		Method testMethod = testClass.getDeclaredMethod(methodName);

		// Act
		TestMethodResult testMethodResult = testMethodRunner.testMethod(testMethod);

		// Assert
		Assert.assertEquals(testMethodResult.getName(), methodName);
		Assert.assertEquals(testMethodResult.getTestVerification(), TestVerification.SUCCESS);
		Assert.assertEquals(testMethodResult.getTime(), testTime);
		Assert.assertEquals(testMethodResult.getException().isPresent(), false);

		verify(timer).start();
		verify(timer).stop();
		verify(timer).getTimeElapsedInMilliseconds(); // To check declared timeout
		verify(timer).getTimeElapsedInSeconds(); // To add to TestMethodResult
	}

	@Test
	public void runTestMethodWithAssertionFailureSuccess() throws Exception {
		// Arrange
		String methodName = "stringLengthSuccess";
		Object testObject = testClass.newInstance();
		testMethodRunner.setTestObject(testObject);
		Method testMethod = testClass.getDeclaredMethod(methodName);

		// Act
		TestMethodResult testMethodResult = testMethodRunner.testMethod(testMethod);

		// Assert
		Assert.assertEquals(testMethodResult.getName(), methodName);
		Assert.assertEquals(testMethodResult.getTestVerification(), TestVerification.FAILURE);
		Assert.assertEquals(testMethodResult.getTime(), testTime);
		Assert.assertEquals(testMethodResult.getException().isPresent(), true);

		verify(timer).start();
		verify(timer).stop();
		verify(timer).getTimeElapsedInSeconds(); // To add to TestMethodResult
	}

	@Test
	public void runTestMethodWithExpectedExceptionSuccess() throws Exception {
		// Arrange
		String methodName = "throwExpectedException";
		Object testObject = testClass.newInstance();
		testMethodRunner.setTestObject(testObject);
		Method testMethod = testClass.getDeclaredMethod(methodName);

		// Act
		TestMethodResult testMethodResult = testMethodRunner.testMethod(testMethod);

		// Assert
		Assert.assertEquals(testMethodResult.getName(), methodName);
		Assert.assertEquals(testMethodResult.getTestVerification(), TestVerification.SUCCESS);
		Assert.assertEquals(testMethodResult.getTime(), testTime);
		Assert.assertEquals(testMethodResult.getException().isPresent(), false);

		verify(timer).start();
		verify(timer).stop();
		verify(timer).getTimeElapsedInSeconds(); // To add to TestMethodResult
	}

	@Test
	public void runTestMethodThatThrowUnexpectedExceptionSuccess() throws Exception {
		// Arrange
		String methodName = "unexpectedException";
		Object testObject = testClass.newInstance();
		testMethodRunner.setTestObject(testObject);
		Method testMethod = testClass.getDeclaredMethod(methodName);

		// Act
		TestMethodResult testMethodResult = testMethodRunner.testMethod(testMethod);

		// Assert
		Assert.assertEquals(testMethodResult.getName(), methodName);
		Assert.assertEquals(testMethodResult.getTestVerification(), TestVerification.FAILURE);
		Assert.assertEquals(testMethodResult.getTime(), testTime);
		Assert.assertEquals(testMethodResult.getException().isPresent(), true);

		verify(timer).start();
		verify(timer).stop();
		verify(timer).getTimeElapsedInSeconds(); // To add to TestMethodResult
	}

	@Test
	public void runTestMethodThatDeclareExceptionButNotThrowItSuccess() throws Exception {
		// Arrange
		String methodName = "exceptionNotHappened";
		Object testObject = testClass.newInstance();
		testMethodRunner.setTestObject(testObject);
		Method testMethod = testClass.getDeclaredMethod(methodName);

		// Act
		TestMethodResult testMethodResult = testMethodRunner.testMethod(testMethod);

		// Assert
		Assert.assertEquals(testMethodResult.getName(), methodName);
		Assert.assertEquals(testMethodResult.getTestVerification(), TestVerification.FAILURE);
		Assert.assertEquals(testMethodResult.getTime(), testTime);
		Assert.assertEquals(testMethodResult.getException().isPresent(), true);

		verify(timer).start();
		verify(timer).stop();
		verify(timer).getTimeElapsedInSeconds(); // To add to TestMethodResult
	}

	@Test
	public void runTestMethodWithBigTimeoutSuccess() throws Exception {
		// Arrange
		String methodName = "bigTimeout";
		Object testObject = testClass.newInstance();
		testMethodRunner.setTestObject(testObject);
		Method testMethod = testClass.getDeclaredMethod(methodName);

		// Act
		TestMethodResult testMethodResult = testMethodRunner.testMethod(testMethod);

		// Assert
		Assert.assertEquals(testMethodResult.getName(), methodName);
		Assert.assertEquals(testMethodResult.getTestVerification(), TestVerification.SUCCESS);
		Assert.assertEquals(testMethodResult.getTime(), testTime);
		Assert.assertEquals(testMethodResult.getException().isPresent(), false);

		verify(timer).start();
		verify(timer).stop();
		verify(timer).getTimeElapsedInMilliseconds(); // To check declared timeout
		verify(timer).getTimeElapsedInSeconds(); // To add to TestMethodResult
	}

	@Test
	public void runTestMethodWithLowTimeoutSuccess() throws Exception {
		// Arrange
		String methodName = "lowTimeout";
		Object testObject = testClass.newInstance();
		testMethodRunner.setTestObject(testObject);
		Method testMethod = testClass.getDeclaredMethod(methodName);

		// Act
		TestMethodResult testMethodResult = testMethodRunner.testMethod(testMethod);

		// Assert
		Assert.assertEquals(testMethodResult.getName(), methodName);
		Assert.assertEquals(testMethodResult.getTestVerification(), TestVerification.FAILURE);
		Assert.assertEquals(testMethodResult.getTime(), testTime);
		Assert.assertEquals(testMethodResult.getException().isPresent(), true);

		verify(timer).start();
		verify(timer).stop();
		verify(timer).getTimeElapsedInMilliseconds(); // To check declared timeout
		verify(timer).getTimeElapsedInSeconds(); // To add to TestMethodResult
	}

}
