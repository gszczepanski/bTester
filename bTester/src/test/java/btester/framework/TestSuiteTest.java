package btester.framework;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import btester.framework.exceptions.BTesterRuntimeException;
import test.domain.NotEnabledTest;
import test.domain.SimpleTestExample;

public class TestSuiteTest {

	private TestSuite testSuite;

	@BeforeMethod
	public void beforeMethod() {
		testSuite = new TestSuite();
	}

	@Test
	public void addUnitTestSuccess() throws Exception {
		testSuite.addUnitTest(SimpleTestExample.class);

		Assert.assertEquals(testSuite.getTestSet().size(), 1);
	}

	@Test(expectedExceptions = BTesterRuntimeException.class)
	public void addUnitTestFailureThrowsException() throws Exception {
		testSuite.addUnitTest(String.class); // Not annotated @TestClass class

		Assert.assertEquals(testSuite.getTestSet().size(), 0);
	}

	@Test
	public void testIsNotAddedToTestSuiteBecauseIsNotEnabled() throws Exception {
		testSuite.addUnitTest(NotEnabledTest.class);

		Assert.assertEquals(testSuite.getTestSet().size(), 0);
	}

}
