package test.domain;

import btester.domain.annotations.TestClass;
import btester.domain.annotations.UnitTest;
import btester.framework.Assert;
import btester.framework.exceptions.AssertFailureException;

@TestClass
public class SimpleTestExample {

	@UnitTest
	public void powSuccess() throws AssertFailureException {
		double a = 2;
		double b = 10;

		double result = Math.pow(a, b);

		Assert.assertEquals(1024.0, result);
	}

	@UnitTest
	public void stringLengthSuccess() throws AssertFailureException {
		String a = "eight";

		Assert.assertEquals(8, a.length()); // Cause AssertFailureException
	}

	@UnitTest(expectedException = NullPointerException.class)
	public void throwExpectedException() throws Exception {
		throw new NullPointerException();
	}

	@UnitTest
	public void unexpectedException() throws Exception {
		throw new NullPointerException();
	}

	@UnitTest(expectedException = NullPointerException.class)
	public void exceptionNotHappened() throws Exception {

	}

	@UnitTest(timeout = 200000)
	public void bigTimeout() throws Exception {

	}

	@UnitTest(timeout = 500)
	public void lowTimeout() throws Exception {

	}
}
