# bTester
Simple Test Framework. 

This is simple unit test framework, that I wrote just for fun and experience. It doesn't intent to compete with other popular Java test frameworks such jUnit or TestNG.
My only goal is to develop some elegant and useful tool, nothing more.

## Installation ##
1. First of all, download current version from here - /lib/bTester.jar.
2. Then add bTester jar to your Build Path. You can do this explicity, or use Maven dependency mechanism.
3. Once you want to use it as Maven dependency, you have to install this jar file in your Maven Local Repository. If you don't know how to do that, check Mavens guide: https://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html .
4. After you installed jar file in your Maven Local Repository, you can declare it as dependency in Maven pom.xml file, using
the name, version, and groupId that you entered.
```xml
<dependency>
  	<groupId>dolphins.from.space</groupId>
	<artifactId>bTester</artifactId>
	<version>0.9-RELEASE</version>
</dependency>
```


## Create Test ##
To run bTester tests you have to take a few steps:

Create test class, and mark class with @TestClass annotation:
```java
@TestClass
public class CoffeMachineTest {
...
```

Create unit test methods within the class. Each method should be marked as @UnitTest method:
```java
  	@UnitTest
	public void makeCoffeSuccess(){
		...
	}
	
	@UnitTest(expectedException=NoElectricityException.class)
	public void makeCoffeWithNoElectricityThrowsException() throws Exception{
		//bTester expects that this method will throw declared exception
	}
	
	@UnitTest(timeout = 5000)
	public void makeCoffeShouldBeQuickSuccess() throws Exception{
	  //bTester expects the test will be performed for less than 5 seconds (5000 ms)
	}
```

You can mark methods with another annotations @BeforeTest, @BeforeMethod, @AfterTest, @AfterMethod, @SecondChance:
```java
  	@BeforeTest
	public void turnOnCoffeMachine(){
		//bTester wil start this method only once, when test class is loaded to bTester
	}
	
	@BeforeMethod
	public void prepareCoffeMachine(){
		//bTester will start this method before each unit test method
	}
	
	@SecondChance
	@UnitTest
	public void makeCoffeTestSuccess(){
		//bTester will give this method a second chance if this method will fail at the first time
	}
	
	@AfterMethod
	public void cleanCoffeMachine(){
		//bTester will start this method after each unit test method 
	}
	
	@AfterTest
	public void turnOffCoffeMachine(){
		//bTester will start this method after all unit test methods 
	}
```

## Run Tests ##

1. Create program class, within main method,
2. Create TestSuite.class instance, and add your test classes to by add() method,
3. Run bTester tests with test() method, passing TestSuite instance as method parameter.

Example: 
```java
  public static void main(String args[]){
		TestSuite testSuite = new TestSuite();
		
		testSuite.addUnitTest(CoffeMachineTest.class);
		testSuite.addUnitTest(TeaMachineTest.class);
		
		BTester.test(testSuite);
	}
```

Happy Testing with bTester! :)
